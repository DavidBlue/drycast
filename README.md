# Drycast Archive

![Drycast Classic Banner](https://i.snap.as/Vy46eZzP.png)

![Drycast Horizontal](https://i.snap.as/OYWLKkSa.jpg)

## Original Description

**Quirky and ready for extended, ironic discussions about memes, your sons and daughters are all squared away and ready to start their first metacognitive cultural radio show. David Blue takes point in this fresh new voyage for the Drywall brand. Your behavior will be deconstructed, and all your assumptions challenged...unsuccessfully. Welcome to Drycast.**

***

| External Links                                               |
| ------------------------------------------------------------ |
| [YouTube Playlist](https://youtube.com/playlist?list=PLiI5-v1HgmCavD7Zi7YInObZ1wWT_tLE_) of the Live Broadcasts |
| [Listen Notes Page](https://www.listennotes.com/podcasts/drycast-extratone--jiCTUNeV51/) (Contains all the episode metadata) |
| Drycast Archive [Anchor Feed](https://anchor.fm/drycast)     |
| [**Drycast Archive**](https://archive.org/details/drycast) on archive.org |
| Web Archive's [last capture](http://web.archive.org/web/20190414093515if_/http://www.extratone.com/audio/drycast/) of Drycast's vertical on Extratone... And [its first](http://web.archive.org/web/20170117234348/http://www.extratone.com/audio/drycast/). |
| Web Archive's [last capture](http://web.archive.org/web/20150831150933/http://www.drywall.ws/drycast/) of the Drywall website's Drycast page |

```powershell
wget --recursive --page-requisites --adjust-extension --span-hosts --convert-links --restrict-file-names=windows --domains web.archive.org --no-parent http://web.archive.org/web/20190126095147/https://extratone.com/
```

